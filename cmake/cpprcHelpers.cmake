cmake_minimum_required(VERSION 3.3)
include(CMakeParseArguments)

function(cpprc_add_resources target)
    if(NOT TARGET ${target})
        message(SEND_ERROR "cpprc_add_resources called on target '${target}' which is not an existing target")
        return()
    endif()

    get_target_property(is_reslib ${target} CPPRC_IS_RESOURCE_LIBRARY)
    if(NOT is_reslib)
        target_link_libraries(${target} libcpprc)
        set_property(TARGET ${target} PROPERTY CPPRC_IS_RESOURCE_LIBRARY TRUE)
    endif()

    set(args ALIAS NAMESPACE)
    cmake_parse_arguments(ARG "" "${args}" "" ${ARGN})

    set(output_cpp "${CMAKE_CURRENT_BINARY_DIR}/_cpprc_${target}.cpp")

    add_custom_command(OUTPUT "${output_cpp}"
        COMMAND cpprc -o "${output_cpp}" ${ARG_UNPARSED_ARGUMENTS}
        COMMENT "Generating ${output_cpp} with cpprc."
        WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
    )

    target_sources(${target} PRIVATE ${output_cpp})
endfunction()