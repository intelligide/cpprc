#include <iostream>
#include <fstream>
#include <string>

#include <stdlib.h>

#include <boost/filesystem.hpp>
#include <args.hxx>

using namespace boost::filesystem;

#define MAX_BYTES_PER_LINE 15

struct res_file_info {
    std::string id;
    std::string filepath;
    std::string filename;

    std::string prefix() const {
        return "f_"+ id + "_";
    }

    std::string dataVarname() const {
        return prefix() + "data";
    }

    std::string beginIteratorVarname() {
        return prefix() + "begin";
    }

    std::string endIteratorVarname() {
        return prefix() + "end";
    }
};

res_file_info computeFileInfo(const std::string& filepath) 
{
    static std::hash<std::string> hash_fn;

    res_file_info fi;
    fi.filepath = absolute(filepath).string();
    fi.filename = path(fi.filepath).filename().string();
    fi.id = std::to_string(hash_fn(fi.filepath)) + "_" + path(fi.filepath).stem().string();

    return fi;
}

std::vector<res_file_info> genFileData(std::ostream& out, const std::vector<std::string>& files) 
{
    std::vector<res_file_info> fileInfoList;
    fileInfoList.reserve(files.size());

    for(std::string filepath: files) {

        res_file_info fi = computeFileInfo(filepath);

        std::ifstream in(fi.filepath, std::ios_base::binary);
        if(!in.is_open()) {
            throw std::system_error(make_error_code(std::errc::no_such_file_or_directory), filepath);
        }

        out << "// " << fi.filepath << std::endl;

        out << "const char " << fi.dataVarname() << "[] = { ";

        int c = 0;
        int byte;
        while ((byte = in.get()) != EOF) {
            
            if(byte != EOF) {
                if(c % MAX_BYTES_PER_LINE == 0) {
                    out << std::endl << "    ";
                }  
                out << "'\\x" << std::hex << byte << "',";
                c++;
            }
        }

        in.close();

        out << " };" << std::endl;

        out << "const char* const " << fi.beginIteratorVarname() << " = " << fi.dataVarname() << ";" << std::endl;
        out << "const char* const " << fi.endIteratorVarname() << " = " << fi.dataVarname() << " + " << std::dec << c << ";" << std::endl;
        out << std::endl;

        fileInfoList.push_back(fi);
    }

    return fileInfoList;
}

void genRootIndex(std::ostream& out, const std::vector<res_file_info>& files)
{
    out << "const cpprc::detail::index_type& get_root_index() {" << std::endl
        << "    static cpprc::detail::directory root_directory;" << std::endl
        << "    static cpprc::detail::file_or_directory root_directory_fod{root_directory};" << std::endl
        << "    static cpprc::detail::index_type root_index;" << std::endl
        << "    root_index.emplace(\"\", &root_directory_fod);" << std::endl;

    for(res_file_info fi : files) {
        out << "    root_index.emplace(" << std::endl
            << "        \"" << fi.filename << "\"," << std::endl
            << "        root_directory.add_file(" << std::endl
            << "            \"" << fi.filename << "\"," << std::endl
            << "            " << fi.beginIteratorVarname() << ", " << std::endl
            << "            " << fi.endIteratorVarname() << std::endl
            << "        )" << std::endl
            << "    );" << std::endl;
    }

    out << "    return root_index;" << std::endl;
    out << "}" << std::endl;
}

void gen(std::ostream& out, const std::vector<std::string>& files)
{
    out << "#include <cpprc/cpprc.h>" << std::endl;
    out << "#include <map>" << std::endl;
    out << "#include <utility>" << std::endl;
    out << std::endl;

    std::vector<res_file_info> fi = genFileData(out, files);

    genRootIndex(out, fi);

    out << std::endl;
    out << "cpprc::embedded_filesystem cpprc::get_filesystem() {" << std::endl;
    out << "    static auto& index = get_root_index();" << std::endl;
    out << "    return cpprc::embedded_filesystem{index};" << std::endl;
    out << "}" << std::endl;
}

int main(int argc, char **argv)
{
    args::ArgumentParser parser("C++ Resource Compiler");
    args::HelpFlag help(parser, "help", "Display this help menu", {'h', "help"});
    args::CompletionFlag completion(parser, {"complete"});
    args::ValueFlag<std::string> output(parser, "output", "", {'o', "output"});
    args::PositionalList<std::string> files(parser, "files", "", args::Options::Required);

    try {
        parser.ParseCLI(argc, argv);
    } catch (const args::Completion& e) {
        std::cout << e.what();
        return EXIT_SUCCESS;
    } catch (const args::Help&) {
        std::cout << parser;
        return EXIT_SUCCESS;
    } catch (const args::ParseError& e) {
        std::cerr << e.what() << std::endl;
        std::cerr << parser;
        return EXIT_FAILURE;
    } catch (args::ValidationError e) {
        std::cerr << e.what() << std::endl;
        std::cerr << parser;
        return EXIT_FAILURE;
    }

    try {
        std::string outputFilePath;
        if(output) {
            outputFilePath = args::get(output);
        } else {
            outputFilePath = "rc.cpp";
        }

        std::ofstream outputfileStream(outputFilePath);
        if(!outputfileStream.is_open()) {
            throw std::system_error(make_error_code(std::errc::io_error), outputFilePath);
        }

        std::ostringstream outContent;
        gen(outContent, args::get(files));
        outputfileStream << outContent.str();

        outputfileStream.close();

        return EXIT_SUCCESS;
    } catch (std::exception e) {
        std::cerr << "cpprc: " << e.what() << std::endl;
        return EXIT_FAILURE;
    }
    
}