#include "cpprc/cpprc.h"

using namespace cpprc;

///////////////////////////////////////////////////////////////////////////////////////////////////
// embedded_filesystem
///////////////////////////////////////////////////////////////////////////////////////////////////

embedded_filesystem::embedded_filesystem(const detail::index_type& index) :
    m_index(&index)
{
}

file embedded_filesystem::open(const std::string& path) const 
{
    auto entry_ptr = get(path);
    if (!entry_ptr || !entry_ptr->is_file()) {
        throw std::system_error(make_error_code(std::errc::no_such_file_or_directory), path);
    }
    auto& dat = entry_ptr->as_file();
    return file{dat.begin_ptr, dat.end_ptr};
}

bool embedded_filesystem::is_file(const std::string& path) const noexcept 
{
    auto entry_ptr = get(path);
    return entry_ptr && entry_ptr->is_file();
}

bool embedded_filesystem::is_directory(const std::string& path) const noexcept 
{
    auto entry_ptr = get(path);
    return entry_ptr && entry_ptr->is_directory();
}

bool embedded_filesystem::exists(const std::string& path) const noexcept {
    return !!get(path);
}

directory_iterator embedded_filesystem::iterate_directory(const std::string& path) const 
{
    auto entry_ptr = get(path);
    if (!entry_ptr) {
        throw std::system_error(make_error_code(std::errc::no_such_file_or_directory), path);
    }
    if (!entry_ptr->is_directory()) {
        throw std::system_error(make_error_code(std::errc::not_a_directory), path);
    }
    return entry_ptr->as_directory().begin();
}

const detail::file_or_directory* embedded_filesystem::get(std::string path) const 
{
    path = detail::normalize_path(path);
    auto found = m_index->find(path);
    if (found == m_index->end()) {
        return nullptr;
    } else {
        return found->second;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// file
///////////////////////////////////////////////////////////////////////////////////////////////////

file::file(iterator beg, iterator end) noexcept : 
    m_begin(beg), 
    m_end(end) 
{
}

file::iterator file::begin() const noexcept 
{ 
    return m_begin; 
}

file::iterator file::cbegin() const noexcept 
{
     return m_begin;
}

file::iterator file::end() const noexcept 
{ 
    return m_end; 
}

file::iterator file::cend() const noexcept 
{ 
    return m_end; 
}

std::size_t file::size() const
{
    return std::distance(begin(), end()); 
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// file_or_directory
///////////////////////////////////////////////////////////////////////////////////////////////////

using namespace cpprc::detail;

file_or_directory::file_or_directory(file_data& f)
{
    _data.file_data = &f;
    m_is_file = true;
}

file_or_directory::file_or_directory(directory& d) 
{
    _data.directory = &d;
    m_is_file = false;
}

bool file_or_directory::is_file() const noexcept 
{
    return m_is_file;
}

bool file_or_directory::is_directory() const noexcept 
{
    return !is_file();
}

const directory& file_or_directory::as_directory() const noexcept 
{
    assert(!is_file());
    return *_data.directory;
}

const file_data& file_or_directory::as_file() const noexcept 
{
    assert(is_file());
    return *_data.file_data;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// file_data
///////////////////////////////////////////////////////////////////////////////////////////////////

file_data::file_data(const char* b, const char* e) : 
    begin_ptr(b), 
    end_ptr(e) 
{
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// directory::iterator
///////////////////////////////////////////////////////////////////////////////////////////////////

directory::iterator::iterator(base_iterator iter, base_iterator end) : 
    m_base_iter(iter), 
    m_end_iter(end) 
{
}

directory::iterator directory::iterator::begin() const noexcept 
{
    return *this;
}

directory::iterator directory::iterator::end() const noexcept 
{
    return iterator(m_end_iter, m_end_iter);
}

directory::iterator::value_type directory::iterator::operator*() const noexcept 
{
    assert(begin() != end());
    return directory_entry(m_base_iter->first, m_base_iter->second);
}

bool directory::iterator::operator==(const iterator& rhs) const noexcept 
{
    return m_base_iter == rhs.m_base_iter;
}

bool directory::iterator::operator!=(const iterator& rhs) const noexcept 
{
    return !(*this == rhs);
}

directory::iterator directory::iterator::operator++() noexcept 
{
    auto cp = *this;
    ++m_base_iter;
    return cp;
}

directory::iterator& directory::iterator::operator++(int) noexcept 
{
    ++m_base_iter;
    return *this;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// directory
///////////////////////////////////////////////////////////////////////////////////////////////////

created_subdirectory directory::add_subdir(std::string name) & 
{
    m_dirs.emplace_back();
    auto& back = m_dirs.back();
    auto& fod = m_index.emplace(name, file_or_directory{back}).first->second;
    return created_subdirectory{back, fod};
}

file_or_directory* directory::add_file(std::string name, const char* begin, const char* end) & 
{
    assert(m_index.find(name) == m_index.end());
    m_files.emplace_back(begin, end);
    return &m_index.emplace(name, file_or_directory{m_files.back()}).first->second;
}

const file_or_directory* directory::get(const std::string& path) const 
{
    auto pair = split_path(path);
    auto child = m_index.find(pair.first);
    if (child == m_index.end()) {
        return nullptr;
    }
    auto& entry  = child->second;
    if (pair.second.empty()) {
        // We're at the end of the path
        return &entry;
    }

    if (entry.is_file()) {
        // We can't traverse into a file. Stop.
        return nullptr;
    }
    // Keep going down
    return entry.as_directory().get(pair.second);
}

directory::iterator directory::begin() const noexcept 
{
    return iterator(m_index.begin(), m_index.end());
}

directory::iterator directory::end() const noexcept 
{
    return iterator();
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// directory_entry
///////////////////////////////////////////////////////////////////////////////////////////////////

directory_entry::directory_entry(std::string filename, const detail::file_or_directory& item) : 
    m_fname(filename),
    m_item(&item)
{
}

const std::string& directory_entry::filename() const & 
{
    return m_fname;
}

std::string directory_entry::filename() const && 
{
    return std::move(m_fname);
}

bool directory_entry::is_file() const 
{
    return m_item->is_file();
}

bool directory_entry::is_directory() const 
{
    return m_item->is_directory();
}
