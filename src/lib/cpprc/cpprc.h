#ifndef CPPRC_H
#define CPPRC_H

#include <cassert>
#include <functional>
#include <iterator>
#include <list>
#include <map>
#include <mutex>
#include <string>
#include <system_error>
#include <type_traits>

namespace cpprc {

class file 
{
public:
    using iterator = const char*;
    using const_iterator = iterator;

    file() = default;
    file(iterator beg, iterator end) noexcept;

    iterator begin() const noexcept;
    iterator cbegin() const noexcept;
    iterator end() const noexcept;
    iterator cend() const noexcept;
    std::size_t size() const;

private:
    const char* m_begin = nullptr;
    const char* m_end = nullptr;
};

class directory_entry;

namespace detail {

inline std::pair<std::string, std::string> split_path(const std::string& path) {
    auto first_sep = path.find("/");
    if (first_sep == path.npos) {
        return std::make_pair(path, "");
    } else {
        return std::make_pair(path.substr(0, first_sep), path.substr(first_sep + 1));
    }
}

inline std::string normalize_path(std::string path) {
    while (path.find("/") == 0) {
        path.erase(path.begin());
    }
    while (!path.empty() && (path.rfind("/") == path.size() - 1)) {
        path.pop_back();
    }
    auto off = path.npos;
    while ((off = path.find("//")) != path.npos) {
        path.erase(path.begin() + off);
    }
    return path;
}

class directory;
class file_data;

class file_or_directory 
{
public:
    explicit file_or_directory(file_data& f);
    explicit file_or_directory(directory& d);

    bool is_file() const noexcept;
    bool is_directory() const noexcept;
    const directory& as_directory() const noexcept;
    const file_data& as_file() const noexcept;

private:
    union _data_t {
        class file_data* file_data;
        class directory* directory;
    } _data;
    bool m_is_file;
};

class file_data 
{
public:
    const char* begin_ptr;
    const char* end_ptr;

    file_data(const file_data&) = delete;
    file_data(const char* b, const char* e);
};

struct created_subdirectory {
    class directory& directory;
    class file_or_directory& index_entry;
};

class directory 
{
    using base_iterator = std::map<std::string, file_or_directory>::const_iterator;
public:
    class iterator 
    {
    public:
        using value_type = directory_entry;
        using difference_type = std::ptrdiff_t;
        using pointer = const value_type*;
        using reference = const value_type&;
        using iterator_category = std::input_iterator_tag;

        iterator() = default;
        explicit iterator(base_iterator iter, base_iterator end);

        iterator begin() const noexcept;

        iterator end() const noexcept;

        value_type operator*() const noexcept;

        bool operator==(const iterator& rhs) const noexcept;

        bool operator!=(const iterator& rhs) const noexcept;

        iterator operator++() noexcept;

        iterator& operator++(int) noexcept;

    private:
        base_iterator m_base_iter;
        base_iterator m_end_iter;
    };

    using const_iterator = iterator;

    directory() = default;
    directory(const directory&) = delete;

    created_subdirectory add_subdir(std::string name) &;

    file_or_directory* add_file(std::string name, const char* begin, const char* end) &;

    const file_or_directory* get(const std::string& path) const;

    iterator begin() const noexcept;

    iterator end() const noexcept;

private:
    std::list<file_data> m_files;
    std::list<directory> m_dirs;
    std::map<std::string, file_or_directory> m_index;
};

using index_type = std::map<std::string, const cpprc::detail::file_or_directory*>;

} // detail

class directory_entry 
{
public:
    directory_entry() = delete;
    explicit directory_entry(std::string filename, const detail::file_or_directory& item);

    const std::string& filename() const &;
    std::string filename() const &&;
    bool is_file() const;
    bool is_directory() const;
private:
    std::string m_fname;
    const detail::file_or_directory* m_item;
};

using directory_iterator = detail::directory::iterator;

class embedded_filesystem 
{
public:
    explicit embedded_filesystem(const detail::index_type& index);

    file open(const std::string& path) const;

    bool is_file(const std::string& path) const noexcept;

    bool is_directory(const std::string& path) const noexcept;

    bool exists(const std::string& path) const noexcept;

    directory_iterator iterate_directory(const std::string& path) const;

private:
    const detail::file_or_directory* get(std::string path) const;

    // Never-null:
    const detail::index_type* m_index;
};

embedded_filesystem get_filesystem();

}

#endif // CPPRC_H