
file(GLOB_RECURSE sources *)

add_library(libcpprc STATIC ${sources})
target_include_directories(libcpprc PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}")

include(GNUInstallDirs)
install(TARGETS libcpprc DESTINATION ${CMAKE_INSTALL_LIBDIR})
install(FILES cpprc/cpprc.h DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/cpprc)
