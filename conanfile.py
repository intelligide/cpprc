from conans import ConanFile, CMake, tools

boostlib_list = ['math', 'wave', 'container', 'contract', 'exception', 'graph', 'iostreams', 'locale', 'log',
                 'program_options', 'random', 'regex', 'mpi', 'serialization',
                 'coroutine', 'fiber', 'context', 'timer', 'thread', 'chrono', 'date_time',
                 'atomic', 'system', 'graph_parallel', 'python',
                 'stacktrace', 'test', 'type_erasure']

class CpprcConan(ConanFile):
    name = "cpprc"
    version = "1.0.0"
    license = "MIT"
    author = "<Put your name here> <And your email here>"
    url = "https://gitlab.com/intelligide/cmrc"
    description = "A Resource Compiler for CPP"
    topics = ("<Put some tag here>", "<here>", "<and here>")
    exports = ["LICENSE.txt"] 
    exports_sources = ["CMakeLists.txt", "src/*", "cmake/*"]
    generators = "cmake"

    settings = "os", "compiler", "build_type", "arch"
    options = {"fPIC": [True, False]}
    default_options = ["fPIC=True"]
    default_options.extend(["boost:without_%s=True" % libname for libname in boostlib_list])
    default_options = tuple(default_options)

    requires = "args/6.2.2@pavel-belikov/stable"
    build_requires = "boost/1.70.0@conan/stable"

    def config_options(self):
        if self.settings.os == 'Windows':
            del self.options.fPIC


    def _configure_cmake(self):
        cmake = CMake(self)
        cmake.definitions["BUILD_TESTS"] = False  # example
        cmake.configure(build_folder=".")
        return cmake

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()

    def package(self):
        self.copy(pattern="LICENSE", dst="licenses")
        cmake = self._configure_cmake()
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)
