# C++RC - A Standalone C++ Resource Compiler

C++RC is a resource compiler that can easily be included in another project.

## What is a "Resource Compiler"?

For the purpose of this project, a *resource compiler* is a tool that will
compile arbitrary data into a program. The program can then read this data from
without needing to store that data on disk external to the program.

Examples use cases:

- Storing a web page tree for serving over HTTP to clients. Compiling the web
  page into the executable means that the program is all that is required to run
  the HTTP server, without keeping the site files on disk separately.
- Storing embedded scripts and/or shaders that support the program, rather than
  writing them in the code as string literals.
- Storing images and graphics for GUIs.

These things are all about aiding in the ease of portability and distribution of
the program, as it is no longer required to ship a plethora of support files
with a binary to your users.

## Installing

### Using Conan

1. Setup the conan remote.

   ```bash
   $ conan remote add intelligide https://api.bintray.com/conan/intelligide/public-conan
   ```

2. Add cpprc to your *conanfile.txt* (or *conanfile.py*).

   ```txt
   [requires]
   libgit2/0.27.7@arsen-studio/stable

   [generators]
   txt
   cmake
   ```

   You can also install it as standalone package:
   ```bash
   $ conan install cpprc/1.0.0@intelligide/stable
   ```

## Usage

### As standalone package

### With CMake

C++RC has an integration with CMake. You can use C++RC easily with CMake module.

1. Load package in CMake with `find_package`:

   ```cmake
   find_package(cpprc)
   ```

2. Once included, add resource files to a library or an executable using `cpprc_add_resources`,
   like this:

   ```cmake
   cpprc_add_resources(target ...)
   ```

   Where `...` is simply a list of files that you wish to compile into the
   resource library.

3. Inside of the source files, any time you wish to use the library, include the
   `cpprc/cpprc.h` header, which will automatically become available to any
   target that links to a generated resource library target, as `my-program`
   does above:

   ```c++
   #include <cpprc/cpprc.h>

   int main() {
       // ...
   }
   ```

4. Obtain a handle to the embedded resource filesystem by calling the
   `get_filesystem()` function in the `cpprc` namespace. It will be
   generated at `cpprc::get_filesystem()`.

   ```c++
   int main() {
       auto fs = cpprc::get_filesystem();
   }
   ```

   You're now ready to work with the files in your resource library! See the
   section on `embedded_filesystem`

## The `cpprc::embedded_filesystem` API

All resource libraries will their own `cpprc::embedded_filesystem`, which can be
accessed with the `get_filesystem()` function.

This class is trivially copyable and destructible, and acts as a handle to the
statically allocated resource library data.

### Methods on `cpprc::embedded_filesystem`

- `open(const std::string& path) -> cpprc::file` - Opens and returns a
  non-directory `file` object at `path`, or throws `std::system_error()` on
  error.
- `is_file(const std::string& path) -> bool` - Returns `true` if the given
  `path` names a regular file, `false` otherwise.
- `is_directory(const std::string& path) -> bool` - Returns `true` if the given
  `path` names a directory. `false` otherwise.
- `exists(const std::string& path) -> bool` returns `true` if the given path
  names an existing file or directory, `false` otherwise.
- `iterate_directory(const std::string& path) -> cpprc::directory_iterator`
  returns a directory iterator for iterating the contents of a directory. Throws
  if the given `path` does not identify a directory.

## Members of `cpprc::file`

- `typename iterator` and `typename const_iterator` - Just `const char*`.
- `begin()/cbegin() -> iterator` - Return an iterator to the beginning of the
  resource.
- `end()/cend() -> iterator` - Return an iterator past the end of the resource.
- `file()` - Default constructor, refers to no resource.

## Members of `cpprc::directory_iterator`

- `typename value_type` - `cpprc::directory_entry`
- `iterator_category` - `std::input_iterator_tag`
- `directory_iterator()` - Default construct.
- `begin() -> directory_iterator` - Returns `*this`.
- `end() -> directory_iterator` - Returns a past-the-end iterator corresponding
  to this iterator.
- `operator*() -> value_type` - Returns the `directory_entry` for which the
  iterator corresponds.
- `operator==`, `operator!=`, and `operator++` - Implement iterator semantics.

## Members of `cpprc::directory_entry`

- `filename() -> std::string` - The filename of the entry.
- `is_file() -> bool` - `true` if the entry is a file.
- `is_directory() -> bool` - `true` if the entry is a directory.

## Additional Options

After calling `cmrc_add_resource_library`, you can add additional resources to
the library using `cmrc_add_resources` with the name of the library and the
paths to any additional resources that you wish to compile in. This way you can
lazily add resources to the library as your configure script runs.

Both `cmrc_add_resource_library` and `cmrc_add_resources` take two additional
keyword parameters:

- `WHENCE` tells CMakeRC how to rewrite the filepaths to the resource files.
  The default value for `WHENCE` is the `CMAKE_CURRENT_SOURCE_DIR`, which is
  the source directory where `cmrc_add_resources` or `cmrc_add_resource_library`
  is called. For example, if you say `cmrc_add_resources(foo images/flower.jpg)`,
  the resource will be accessible via `cmrc::open("images/flower.jpg")`, but
  if you say `cmrc_add_resources(foo WHENCE images images/flower.jpg)`, then
  the resource will be accessible only using `cmrc::open("flower.jpg")`, because
  the `images` directory is used as the root where the resource will be compiled
  from.

  Because of the file transformation limitations, `WHENCE` is _required_ when
  adding resources which exist outside of the source directory, since CMakeRC
  will not be able to automatically rewrite the file paths.

- `PREFIX` tells CMakeRC to prepend a directory-style path to the resource
  filepath in the resulting binary. For example,
  `cmrc_add_resources(foo PREFIX resources images/flower.jpg)` will make the
  resource accessible using `cmrc::open("resources/images/flower.jpg")`. This is
  useful to prevent resource libraries from having conflicting filenames. The
  default `PREFIX` is to have no prefix.

The two options can be used together to rewrite the paths to your heart's
content:

```cmake
cmrc_add_resource_library(
    flower-images
    NAMESPACE flower
    WHENCE images
    PREFIX flowers
    images/rose.jpg
    images/tulip.jpg
    images/daisy.jpg
    images/sunflower.jpg
    )
```

```c++
int foo() {
    auto fs = cmrc::flower::get_embedded_filesystem();
    auto rose = fs.open("flowers/rose.jpg");
}
```