#include <iostream>

#include <cpprc/cpprc.h>

int main() {
    auto fs = cpprc::get_filesystem();
    auto data = fs.open("hello.txt");
    std::cout << std::string(data.begin(), data.end()) << std::endl;

    return EXIT_SUCCESS;
}
